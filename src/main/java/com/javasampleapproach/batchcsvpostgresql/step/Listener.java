package com.javasampleapproach.batchcsvpostgresql.step;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

import com.javasampleapproach.batchcsvpostgresql.dao.DebtorDao;
import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

public class Listener extends JobExecutionListenerSupport {
	private static final Logger log = LoggerFactory.getLogger(Listener.class);

	private final DebtorDao debtorDao;

	public Listener(DebtorDao debtorDao) {
		this.debtorDao = debtorDao;
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("Finish Job! Check the results");

			List<Debtor> debtors = debtorDao.loadAllDebtors();

			for (Debtor debtor : debtors) {
				log.info("Found <" + debtor + "> in the database.");
			}
		}
	}
}
