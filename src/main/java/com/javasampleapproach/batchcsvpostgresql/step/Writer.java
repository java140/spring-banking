package com.javasampleapproach.batchcsvpostgresql.step;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import com.javasampleapproach.batchcsvpostgresql.dao.DebtorDao;
import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

public class Writer implements ItemWriter<Debtor> {

	private final DebtorDao debtorDao;

	public Writer(DebtorDao debtorDao) {
		this.debtorDao = debtorDao;
	}

	@Override
	public void write(List<? extends Debtor> debtors) throws Exception {
		debtorDao.insert(debtors);
	}

}
