package com.javasampleapproach.batchcsvpostgresql.step;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

public class Processor implements ItemProcessor<Debtor, Debtor> {

	private static final Logger log = LoggerFactory.getLogger(Processor.class);

	@Override
	public Debtor process(Debtor debtor) throws Exception {
		Random r = new Random();
		
		final String firstName = debtor.getFirstName().toUpperCase();
		final String lastName = debtor.getLastName().toUpperCase();

		final Debtor fixedDebtor = new Debtor(r.nextLong(), firstName, lastName);

		log.info("Converting (" + debtor + ") into (" + fixedDebtor + ")");

		return fixedDebtor;
	}
}
