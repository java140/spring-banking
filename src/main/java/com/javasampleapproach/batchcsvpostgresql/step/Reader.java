package com.javasampleapproach.batchcsvpostgresql.step;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;

import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

public class Reader {
	public static FlatFileItemReader<Debtor> reader(String path) {
		FlatFileItemReader<Debtor> reader = new FlatFileItemReader<Debtor>();

		reader.setResource(new ClassPathResource(path));
		reader.setLineMapper(new DefaultLineMapper<Debtor>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "id", "firstName", "lastName" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Debtor>() {
					{
						setTargetType(Debtor.class);
					}
				});
			}
		});
		return reader;
	}
}
