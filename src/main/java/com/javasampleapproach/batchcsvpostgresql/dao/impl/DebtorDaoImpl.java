package com.javasampleapproach.batchcsvpostgresql.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.javasampleapproach.batchcsvpostgresql.dao.DebtorDao;
import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

@Repository
public class DebtorDaoImpl extends JdbcDaoSupport implements DebtorDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public void insert(List<? extends Debtor> debtors) {
		String sql = "INSERT INTO debtor " + "(id, first_name, last_name) VALUES (?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Debtor debtor = debtors.get(i);
				ps.setLong(1, debtor.getId());
				ps.setString(2, debtor.getFirstName());
				ps.setString(3, debtor.getLastName());
			}

			public int getBatchSize() {
				return debtors.size();
			}
		});

	}

	@Override
	public List<Debtor> loadAllDebtors() {
		String sql = "SELECT * FROM debtor";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<Debtor> result = new ArrayList<Debtor>();
		for (Map<String, Object> row : rows) {
			Debtor debtor = new Debtor();
			debtor.setId((Long) row.get("id"));
			debtor.setFirstName((String) row.get("first_name"));
			debtor.setLastName((String) row.get("last_name"));
			result.add(debtor);
		}

		return result;
	}
}
