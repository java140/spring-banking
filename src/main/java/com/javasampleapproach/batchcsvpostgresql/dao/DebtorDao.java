package com.javasampleapproach.batchcsvpostgresql.dao;

import java.util.List;

import com.javasampleapproach.batchcsvpostgresql.model.Debtor;

public interface DebtorDao {
	public void insert(List<? extends Debtor> debtors);
	List<Debtor> loadAllDebtors();
}
