DROP TABLE IF EXISTS debtor;

CREATE TABLE debtor  (
    id Bigserial PRIMARY KEY NOT NULL,
    first_name VARCHAR(20),
    last_name VARCHAR(20)
);